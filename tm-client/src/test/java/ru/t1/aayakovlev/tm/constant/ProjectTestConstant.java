package ru.t1.aayakovlev.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;

public final class ProjectTestConstant {

    public final static int INDEX = 0;

    public final static int INDEX_FOR_REMOVE = 1;

    @NotNull
    public final static String NAME = "Changed Name";

    @NotNull
    public final static String DESCRIPTION = "Changed Description";

    @NotNull
    public final static Status STATUS = Status.COMPLETED;

}
