package ru.t1.aayakovlev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.List;

@Component
public final class SystemArgumentListListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument description.";

    @NotNull
    public static final String NAME = "argument";

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@systemArgumentListListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[ARGUMENT LIST]");
        listeners.stream()
                .filter((c) -> c.getArgument() != null)
                .filter((c) -> !c.getArgument().isEmpty())
                .forEachOrdered((c) -> System.out.println(c.getArgument() + ": " + c.getDescription()));
    }

}
