package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    @Nullable
    private List<UserDTO> users;

    public AbstractUserResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public AbstractUserResponse(@Nullable final List<UserDTO> users) {
        this.users = users;
    }

}
