package ru.t1.aayakovlev.tm.exception.auth;

public final class AccessDeniedException extends AbstractAuthException {

    public AccessDeniedException() {
        super("Error! You do not have permission for execute this command...");
    }

}
