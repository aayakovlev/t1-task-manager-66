package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

    @Nullable
    private List<TaskDTO> tasks;

    public AbstractTaskResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

    public AbstractTaskResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
