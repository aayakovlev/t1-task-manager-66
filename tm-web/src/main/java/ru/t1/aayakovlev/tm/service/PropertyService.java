package ru.t1.aayakovlev.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseURL;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.hb2mdll']}")
    private String databaseHBM2DLL;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @Value("#{environment['database.init_token']}")
    private String databaseInitToken;

    @Value("#{environment['database.schema']}")
    private String databaseSchema;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String useSecondCache;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String useQueryCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String useMinimalPuts;

    @Value("#{environment['database.cache.use_region_prefix']}")
    private String useRegionPrefix;

    @Value("#{environment['database.cache.use_provided_configuration_file_resource_path']}")
    private String HZConfFile;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String factoryClass;

    @Value("#{environment['database.username']}")
    private String databaseUser;

}
