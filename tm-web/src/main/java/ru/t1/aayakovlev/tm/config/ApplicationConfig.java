package ru.t1.aayakovlev.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.aayakovlev.tm")
public final class ApplicationConfig {

}
