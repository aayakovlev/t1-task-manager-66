package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public final class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService service;

    @Override
    @GetMapping("/count")
    public long count() throws AbstractException {
        return service.count();
    }

    @Override
    @DeleteMapping("/delete")
    public void deleteAll() {
        service.deleteAll();
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() throws AbstractException {
        return service.findAll();
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @PutMapping("/save")
    public TaskDTO save(@RequestBody @NotNull final TaskDTO task) throws EntityEmptyException {
        return service.save(task);
    }

    @NotNull
    @Override
    @PostMapping("/save/{id}")
    public TaskDTO update(@RequestBody @NotNull final TaskDTO task) throws EntityEmptyException {
        return service.save(task);
    }

}
