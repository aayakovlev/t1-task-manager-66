package ru.t1.aayakovlev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    long count();

    @Transactional
    void deleteAll();

    @Transactional
    void deleteById(@Nullable final String id) throws AbstractException;

    boolean existsById(@Nullable final String id) throws AbstractException;

    @NotNull
    List<Task> findAll() throws AbstractException;

    @NotNull
    List<Task> findAllByProjectId(@Nullable final String projectId) throws AbstractException;

    @NotNull
    Task findById(@Nullable final String id) throws AbstractException;

    @NotNull
    Task save(@Nullable final Task task) throws EntityEmptyException;

}
