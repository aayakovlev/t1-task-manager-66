package ru.t1.aayakovlev.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.dto.model.AbstractExtendedModelDTO;

@Repository
@Scope("prototype")
public interface ExtendedDTORepository<E extends AbstractExtendedModelDTO> extends BaseDTORepository<E> {

}
