package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends ExtendedDTORepository<ProjectDTO> {

    long countByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<ProjectDTO> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
