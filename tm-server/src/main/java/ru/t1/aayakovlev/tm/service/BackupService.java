package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface BackupService {

    @NotNull
    Boolean getBackupEnabled();

}
