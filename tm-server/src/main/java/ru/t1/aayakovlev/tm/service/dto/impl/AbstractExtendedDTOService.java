package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.AbstractExtendedModelDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.repository.dto.ExtendedDTORepository;
import ru.t1.aayakovlev.tm.service.dto.ExtendedDTOService;

import java.util.*;

@Service
@NoArgsConstructor
public abstract class AbstractExtendedDTOService<E extends AbstractExtendedModelDTO, R extends ExtendedDTORepository<E>>
        extends AbstractBaseDTOService<E, R> implements ExtendedDTOService<E> {

    @NotNull
    protected abstract ExtendedDTORepository<E> getRepository();

    @NotNull
    @Override
    @Transactional
    public E save(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return getRepository().save(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<E> add(@Nullable final String userId, @Nullable final Collection<E> models)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        for (@NotNull final E entity : models) {
            entity.setUserId(userId);
            @NotNull final E resultEntity = getRepository().save(entity);
            resultEntities.add(resultEntity);
        }
        return resultEntities;
    }

    @NotNull
    @Override
    @Transactional
    public E update(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(userId, model.getId())) throw new EntityNotFoundException();
        return update(model);
    }

}
